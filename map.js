const markerPosition = [8.81752, 47.22331];
const zoom = 18;

var map = new ol.Map({
    target: 'map',
    layers: [
        new ol.layer.Tile({
            // if you want to use the default OSM tiles, you can just use the following line.
            // source: new ol.source.OSM()
            // using this, you can remove the attribution as it is already included.
            source: new ol.source.XYZ({
                url: 'http://tile.osm.ch/osm-swiss-style/{z}/{x}/{y}.png',
                attributions: [
                    `<a id="problem-attribution" href="http://www.openstreetmap.org/fixthemap?lat=${markerPosition[1]}&lon=${markerPosition[0]}&zoom=${zoom}">Report a problem</a> |`,
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                ] // You can use HTML in here!
            })
        }),
        createMarker()
    ],
    view: new ol.View({
        zoom: zoom,
        center: ol.proj.fromLonLat(markerPosition),
        maxZoom: 21
    }),
    controls: ol.control.defaults({
        attributionOptions: ({
            collapsible: false
        })
    }),
});

function createMarker() {
    const vectorSource = new ol.source.Vector({
        features: [
            new ol.Feature({
                geometry: new ol.geom.Point(
                    ol.proj.fromLonLat(markerPosition),
                )
            })
        ]
    });

    return new ol.layer.Vector({
        title: 'Company Location',
        source: vectorSource,
        style: new ol.style.Style({
            image: new ol.style.Icon({
                src: 'img/lfmarker.png',
                anchor: [0.5, 1] // 0.5: middle on horizontal axis, 1: bottom of vertical axis.
            }),
        })
    });
}

// code for popup
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

var overlay = new ol.Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    },
    offset: [1, -30], // offset in pixels, [horizontal, vertical]
});
map.addOverlay(overlay);

// Have the popup open when the page loads
content.innerHTML = 'OST Campus RJ';
overlay.setPosition(ol.proj.fromLonLat(markerPosition));

closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

map.on('singleclick', function (event) {
    if (map.hasFeatureAtPixel(event.pixel) === true) {
        var coordinate = event.coordinate;

        content.innerHTML = 'OST Campus RJ';
        overlay.setPosition(coordinate);
    } else {
        overlay.setPosition(undefined);
        closer.blur();
    }
});

// update 'Report a problem' link
map.on('moveend', function () {
    document.getElementById('problem-attribution').setAttribute('href', createProblemLink());
});

function createProblemLink() {
    var coords = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
    var currentZoom = map.getView().getZoom();

    return `http://www.openstreetmap.org/fixthemap?lat=${coords[1]}&lon=${coords[0]}&zoom=${currentZoom}`;
}